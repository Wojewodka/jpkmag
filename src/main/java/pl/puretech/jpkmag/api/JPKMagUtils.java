package pl.puretech.jpkmag.api;

import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static lombok.AccessLevel.PRIVATE;

/**
 * Utility class used by XJB bindings and XSLT, so please be careful if you would to remove any of below methods.
 * @author ewojewodka
 * @version 0.1.0
 */
@NoArgsConstructor(access = PRIVATE)
public class JPKMagUtils {

  private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

  @NonNull
  public static final URL SCHEMA_RESOURCE = JPKMagUtils.class.getClassLoader().getResource("xsd/Schemat_JPK_MAG_v1-0.xsd");

  /**
   *  These method is using by xjc for printing and parsing date in xml. <br/>
   *  <b>xmlType = TData</b>
   * @see #parseToLocalDate(String)
   */
  public static String toYYYYMMDD(LocalDate cal) {
    return cal.format(DATE_FORMATTER);
  }

  /**
   *  These method is using by xjc for printing and parsing date in xml. </br>
   *  <b>xmlType = TData</b>
   * @see #toYYYYMMDD(LocalDate)
   */
  public static LocalDate parseToLocalDate(String date) {
    return LocalDate.from(DATE_FORMATTER.parse(date));
  }

  /**
   *  These method is using by xjc for printing and parsing date time in xml. <br/>
   *  <b>xmlType = TDataCzas</b>
   */
  public static LocalDateTime parseToLocalDateTime(String dateTime) {
    return LocalDateTime.from(DATETIME_FORMATTER.parse(dateTime));
  }

  public static boolean isEmpty(String string) {
    return string == null || string.trim().length() == 0;
  }

  public static String nullIfEmpty(String value) {
    return isEmpty(value) ? null : value;
  }

}
