package pl.puretech.jpkmag.api.service;

import lombok.Cleanup;
import lombok.NonNull;
import pl.puretech.jpkmag.api.JPKMagException;
import pl.puretech.jpkmag.api.documentbuilders.JPKMagBuilder;
import pl.puretech.jpkmag.internal.JPK;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;

import static java.lang.String.valueOf;

/**
 * HTML implementation of {@link JPKMagGenerator} for {@link JPK}. <br/>
 * Data is validate by XSD schema. <br/>
 * HTML is generated by XSLT transformer - output is HTML preview with tables and tabs.
 *
 * @author ewojewodka
 * @version 0.1.1
 */
public class JPKMagHTMLGenerator extends AbstractJPKMagGenerator {

  @NonNull
  private static final URL XSLT_PREVIEW_TEMPLATE = JPKMagHTMLGenerator.class.getClassLoader().getResource("xslt/preview-template.xslt");

  @Override
  public void generate(JPKMagBuilder builder, OutputStream output) throws JPKMagException {
    try {
      JPK jpk = builder.build();
      validate(jpk);

      JAXBContext context = JAXBContext.newInstance(JPK.class);

      TransformerFactory factory = TransformerFactory.newInstance();
      factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

      @Cleanup InputStream inputStream = XSLT_PREVIEW_TEMPLATE.openStream(); // NOSONAR
      Source xslt = new StreamSource(inputStream, XSLT_PREVIEW_TEMPLATE.toURI().toString());
      Transformer transformer = factory.newTransformer(xslt);

      Marshaller marshaller = context.createMarshaller();
      Path tmpfile = Files.createTempFile(valueOf(LocalDateTime.now().getNano()), null);
      marshaller.marshal(jpk, tmpfile.toFile());

      Source source = new StreamSource(tmpfile.toFile());
      transformer.transform(source, new StreamResult(output));

    } catch (Exception e) {
      throw new JPKMagException(e);
    }

  }

}
