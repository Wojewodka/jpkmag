package pl.puretech.jpkmag.api.service;

import org.xml.sax.SAXException;
import pl.puretech.jpkmag.api.JPKMagNotValidException;
import pl.puretech.jpkmag.internal.JPK;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;

import static java.util.Objects.requireNonNull;
import static pl.puretech.jpkmag.api.JPKMagUtils.SCHEMA_RESOURCE;

abstract class AbstractJPKMagGenerator implements JPKMagGenerator {

  protected void validate(JPK jpk) throws JAXBException, SAXException, JPKMagNotValidException, IOException {
    JAXBContext context = JAXBContext.newInstance(JPK.class);
    JAXBSource jaxbContext = new JAXBSource(context, jpk);

    SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    Schema schema = sf.newSchema(requireNonNull(SCHEMA_RESOURCE));

    Validator validator = schema.newValidator();
    try {
      validator.validate(jaxbContext);
    } catch (SAXException e) {
      throw new JPKMagNotValidException(e);
    }
  }

}
