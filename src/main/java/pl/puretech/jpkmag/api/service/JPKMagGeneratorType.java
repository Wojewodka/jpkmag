package pl.puretech.jpkmag.api.service;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enumeration which provide easy access to {@link JPKMagGenerator} implementations.
 *
 * @author ewojewodka
 * @version 0.1.0
 */
@Getter
@AllArgsConstructor
public enum JPKMagGeneratorType {

  /**
   * @see JPKMagXMLGenerator
   */
  XML(new JPKMagXMLGenerator(), ".xml"),
  HTML(new JPKMagHTMLGenerator(), ".html");

  /**
   * {@link JPKMagGenerator} implementation instance.
   */
  private JPKMagGenerator generator;
  private String extension;

}
