package pl.puretech.jpkmag.api.service;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.xml.sax.SAXException;
import pl.puretech.jpkmag.api.JPKMagException;
import pl.puretech.jpkmag.api.documentbuilders.JPKMagBuilder;
import pl.puretech.jpkmag.internal.JPK;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Basic implementation of {@link JPKMagGenerator} for XML. <br/>
 * Data is validate by XSD schema.
 *
 * @see pl.puretech.jpkmag.api.JPKMagUtils#SCHEMA_RESOURCE
 * @author ewojewodka
 * @version 0.1.0
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class JPKMagXMLGenerator extends AbstractJPKMagGenerator {

  private static final String XML_GENERATOR_EXCEPTION_MSG = "Cannot generate XML for JPK_MAG";

  @Override
  public void generate(JPKMagBuilder builder, OutputStream output) throws JPKMagException {
    try {
      JPK jpk = builder.build();
      validate(jpk);

      JAXBContext context = JAXBContext.newInstance(JPK.class);

      Marshaller marshaller = context.createMarshaller();
      marshaller.marshal(jpk, output);
    } catch (JAXBException | SAXException | IOException e) {
      throw new JPKMagException(XML_GENERATOR_EXCEPTION_MSG, e);
    }
  }
}
