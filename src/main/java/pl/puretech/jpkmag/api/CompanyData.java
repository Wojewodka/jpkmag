package pl.puretech.jpkmag.api;

/**
 * Simple interface for storing data about company. <br/>
 * <i>Note that: if this data is used to create xml elements then data from getters should be in correct format with XSD schema.</i>
 * @see CompanyAddress
 * @author ewojewodka
 * @version 0.1.0
 */
public interface CompanyData {

  /**
   * @return nip
   */
  String getVatId();

  /**
   * @return pełna nazwa
   */
  String getFullName();

  /**
   * @return regon
   */
  String getRegon();

  /**
   *
   * @see CompanyAddress
   * @return adres
   */
  CompanyAddress getCompanyAddress();
}
