package pl.puretech.jpkmag.api;

import pl.puretech.jpkmag.internal.TKodKraju;

/**
 * Simple interface which allow to easy mapping data to jpk_mag (or other document) <br/>
 * <i>Note that: if this data is used to create xml elements then data from getters should be in correct format with XSD schema.</i>
 * @see CompanyData
 * @author ewojewodka
 * @version 0.1.0
 */
public interface CompanyAddress {

  /**
   * @return kod kraju
   * @see TKodKraju
   */
  String getCountryCode();

  /**
   * @return wojewodztwo
   */
  String getProvince();

  /**
   * @return gmina
   */
  String getMunicipality();

  /**
   * @return powiat
   */
  String getCounty();

  /**
   * @return ulica
   */
  String getStreet();

  /**
   * It's {@link String} because there may be i.e. 1A
   * @return nr domu
   */
  String getHouse();

  /**
   * It's {@link String} because there may be i.e. 1A or 4-5
   * @return nr lokalu
   */
  String getApartment();

  /**
   * @return miejscowosc
   */
  String getPostCity();

  /**
   * @return kod pocztowy
   */
  String getPostalCode();

  /**
   * @return poczta
   */
  String getPostOffice();

}
