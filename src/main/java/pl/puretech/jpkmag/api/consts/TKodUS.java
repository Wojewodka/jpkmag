package pl.puretech.jpkmag.api.consts;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import pl.puretech.jpkmag.api.documentbuilders.JPKMagBuilder;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import static java.util.Collections.unmodifiableCollection;
import static java.util.Objects.requireNonNull;

/**
 * Representation of TKodUS for JPK_MAG.
 *
 * @see #getByCode(String)
 * @author ewojewodka
 * @version 0.1.0
 */
@Data(staticConstructor = "of")
public class TKodUS {

  private final String code;
  private final String label;

  /**
   * Return original data of all TKodUS. <b>Collection is unmodifiable and sorted by codes.</b>
   */
  public static Collection<TKodUS> getAll() {
    return TKodUSSupplier.data;
  }

  /**
   * Return {@link TKodUS} with specific code. <br/>
   * If object is not found then throw {@link IllegalArgumentException}
   *
   * @throws IllegalArgumentException if object is not found
   */
  public static TKodUS getByCode(String code) {
    return getAll().stream()
        .filter(tkodUs -> tkodUs.getCode().equals(code))
        .findFirst()
        .orElseThrow(IllegalArgumentException::new);
  }

}

/**
 * Type for parsing XSD file for extract KodUS.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class TKodUSSupplier {

  static final Collection<TKodUS> data;
  private static final String XSD_PATH = "xsd/KodyUrzedowSkarbowych_v4-0E.xsd";
  private static final String ENUMERATION_XPATH = "schema/simpleType/restriction/enumeration";
  private static final String ENUMERATION_LABEL_XPATH = "./annotation/documentation";

  static {
    List<TKodUS> codes = new ArrayList<>();

    try {
      URL resource = JPKMagBuilder.class.getClassLoader().getResource(XSD_PATH);

      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      Document doc = dbFactory.newDocumentBuilder().parse(requireNonNull(resource).openStream());

      doc.getDocumentElement().normalize();

      XPath xpath = XPathFactory.newInstance().newXPath();
      XPathExpression expr = xpath.compile(ENUMERATION_XPATH);
      NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

      expr = xpath.compile(ENUMERATION_LABEL_XPATH);

      for (int i = 0; i < nodes.getLength(); i++) {
        Node codeAsNode = nodes.item(i);

        String code = codeAsNode.getAttributes().getNamedItem("value").getNodeValue();
        String label = (String) expr.evaluate(codeAsNode, XPathConstants.STRING);

        codes.add(TKodUS.of(code, label));
      }


    } catch (Exception e) {
      e.printStackTrace();
    }
    codes.sort(Comparator.comparing(TKodUS::getCode));

    data = unmodifiableCollection(codes);
  }

}
