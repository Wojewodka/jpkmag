package pl.puretech.jpkmag.api.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * CelZlozenia JPK_MAG
 * Please look at <a href="https://www.gov.pl/web/kas/struktury-jpk">JPK documentation</a>
 * for more details. <br/>
 *
 * @implNote right now KOREKTA_DEKLARACJI is commented because Ministerstwo Finansów doesn't support this type. (09.08.2019)
 * @author ewojewodka
 * @version 0.1.0
 */
@Getter
@AllArgsConstructor
public enum CelZlozenia {
  /**
   * Złożenie po raz pierwszy deklaracji za dany okres
   */
  PIERWSZA_DEKLARACJA("1", "jpk-mag.cel-zlozenia.pierwsza-deklaracja");
  /**
   * Korekta deklaracji za dany okres
   * @implNote right now it's commented because Ministerstwo Finansów doesn't support this type. (09.08.2019)
   */
//  KOREKTA_DEKLARACJI("2", "jpk-mag.cel-zlozenia.korekta");

  private String xmlValue;
  private String labelCode;

  public static CelZlozenia getByValue(String value) {
    return Stream.of(values())
        .filter(enumValue -> enumValue.xmlValue.equals(value))
        .findFirst().orElseThrow(() -> new IllegalArgumentException("No enum constant with value " + value));
  }

}
