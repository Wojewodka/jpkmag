package pl.puretech.jpkmag.api;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Simple interface which will be mapped to JPK_MAG <i>XXWartosc</i>.
 * @see pl.puretech.jpkmag.internal.JPK.PZ.PZWartosc
 * @see pl.puretech.jpkmag.internal.JPK.WZ.WZWartosc
 * @see pl.puretech.jpkmag.internal.JPK.RW.RWWartosc
 * @author ewojewodka
 * @version 0.1.0
 */
public interface JPKMagDocumentValue {

  /**
   * Unique document number/code i.e. 1/19/PZ
   * @return number
   */
  String getNumber();

  /**
   * Document creation date.
   * @return data
   */
  LocalDate getCreatedAt();

  /**
   * Document value
   * @return document value
   */
  BigDecimal getValue();

  /**
   * Document date, it may be admission date or release date
   * @return document action date
   */
  LocalDate getDocumentDate();

  /**
   * It may be supplier or receiver.
   * @return target name
   */
  String getTargetFullName();

  /**
   * Invoice code, i.e. FV001/09/2019
   * @see #getInvoiceDate()
   * @return invoice code linked with document
   */
  @Nullable
  String getInvoiceCode();

  /**
   * Invoice date. Should not be null if {@link #getInvoiceCode()} has value.
   * @see #getInvoiceCode()
   * @return invoice date linked with document
   */
  @Nullable
  LocalDate getInvoiceDate();

}
