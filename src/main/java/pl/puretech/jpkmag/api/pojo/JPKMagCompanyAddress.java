package pl.puretech.jpkmag.api.pojo;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import pl.puretech.jpkmag.api.CompanyAddress;
import pl.puretech.jpkmag.internal.TKodKraju;

/**
 * Please look at <a href="https://www.gov.pl/web/kas/struktury-jpk">JPK documentation</a>
 * for field descriptions. <br/>
 *
 * <b>All fields are required</b>
 * @see JPKMagCompanyData
 * @see CompanyAddress
 * @author ewojewodka
 * @version 0.1.0
 */
@Data
@Builder
public class JPKMagCompanyAddress implements CompanyAddress {

  @NonNull @Builder.Default private TKodKraju kodKraju = TKodKraju.PL;
  @NonNull private String wojewodztwo;
  @NonNull private String powiat;
  @NonNull private String gmina;
  @NonNull private String ulica;
  @NonNull private String nrDomu;
  @NonNull private String nrLokalu;
  @NonNull private String miejscowosc;
  @NonNull private String kodPocztowy;
  @NonNull private String poczta;


  @Override
  public String getCountryCode() {
    return kodKraju.value();
  }

  @Override
  public String getProvince() {
    return wojewodztwo;
  }

  @Override
  public String getMunicipality() {
    return gmina;
  }

  @Override
  public String getCounty() {
    return powiat;
  }

  @Override
  public String getStreet() {
    return ulica;
  }

  @Override
  public String getHouse() {
    return nrDomu;
  }

  @Override
  public String getApartment() {
    return nrLokalu;
  }

  @Override
  public String getPostCity() {
    return miejscowosc;
  }

  @Override
  public String getPostalCode() {
    return kodPocztowy;
  }

  @Override
  public String getPostOffice() {
    return poczta;
  }
}
