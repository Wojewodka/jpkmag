package pl.puretech.jpkmag.api.pojo;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import pl.puretech.jpkmag.api.CompanyData;
import pl.puretech.jpkmag.api.consts.CelZlozenia;
import pl.puretech.jpkmag.api.consts.TKodUS;
import pl.puretech.jpkmag.internal.CurrCodeType;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Simple POJO with builder for easy create JPK data. <br/>
 * Please look at <a href="https://www.gov.pl/web/kas/struktury-jpk">JPK documentation</a>
 * for field descriptions. <br/>
 * Note that fields are required, but {@link #wariantFormularza}, {@link #dataWytworzenia} and {@link #domyslnyKodWaluty} has default values.
 *
 * @see JPKMagCompanyAddress
 * @see CompanyData
 * @author ewojewodka
 * @version 0.1.0
 */
@Builder
@Getter
public class JPKMagTemplateData {

  @NonNull @Builder.Default private byte wariantFormularza = 1;
  @NonNull private CelZlozenia celZlozenia;
  @NonNull @Builder.Default private LocalDateTime dataWytworzenia = LocalDateTime.now();
  @NonNull private LocalDate dataOd;
  @NonNull private LocalDate dataDo;
  @NonNull @Builder.Default private CurrCodeType domyslnyKodWaluty = CurrCodeType.PLN;
  @NonNull private TKodUS kodUrzedu;
  @NonNull private CompanyData podmiot;
  @NonNull private String magazyn;

}
