package pl.puretech.jpkmag.api.pojo;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import pl.puretech.jpkmag.api.CompanyAddress;
import pl.puretech.jpkmag.api.CompanyData;

/**
 * Please look at <a href="https://www.gov.pl/web/kas/struktury-jpk">JPK documentation</a>
 * for field descriptions. <br/>
 * <b>All fields are required</b>
 * @see JPKMagCompanyAddress
 * @see CompanyAddress
 * @see JPKMagTemplateData
 * @author ewojewodka
 * @version 0.1.0
 */
@Getter
@Builder
public class JPKMagCompanyData implements CompanyData {

  @NonNull private String nip;
  @NonNull private String pelnaNazwa;
  @NonNull private String regon;
  @NonNull private CompanyAddress adresPodmiotu;

  @Override
  public String getVatId() {
    return nip;
  }

  @Override
  public String getFullName() {
    return pelnaNazwa;
  }

  @Override
  public CompanyAddress getCompanyAddress() {
    return adresPodmiotu;
  }
}
