package pl.puretech.jpkmag.api;

import java.math.BigDecimal;

/**
 * Simple interface for getting JPK_MAG XXWiersz.
 * @see JPKMagDocumentValue
 * @author ewojewodka
 * @version 0.1.0
 */
public interface JPKMagDocumentValuePosition {

  /**
   * @return numer dokumentu
   */
  String getDocumentNumber();

  /**
   * @return kod pozycji/towaru
   */
  String getPositionCode();

  /**
   * @return nazwa pozycji/towaru
   */
  String getName();

  /**
   * @return ilosc, np.: wydana lub przyjęta
   */
  BigDecimal getAmount();

  /**
   * @return jednostka miary pozycji/towaru
   */
  String getUnit();

  /**
   * @return cena jednostkowa pozycji/towaru
   */
  BigDecimal getPrice();

  /**
   * @return wartosc pozycji/towaru
   */
  BigDecimal getValue();

}
