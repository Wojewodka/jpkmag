package pl.puretech.jpkmag.api;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.xml.sax.SAXException;
import pl.puretech.jpkmag.internal.JPK;

/**
 * Specific implementation of {@link JPKMagException} which is thrown if {@link JPK} validation with
 * current schema is failed.
 *
 * @author ewojewodka
 * @since 09.08.2019
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class JPKMagNotValidException extends JPKMagException {
  private static final long serialVersionUID = 7731213950900133729L;

  /**
   * @implNote please do not creating more constructors if your validator doesn't thrown other type of {@link Exception}
   * @param cause {@link SAXException} which is thrown by XSD schema validator.
   */
  public JPKMagNotValidException(SAXException cause) {
    super("JPK structure is not valid.", cause);
  }

}
