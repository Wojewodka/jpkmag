package pl.puretech.jpkmag.api.documentbuilders;

import pl.puretech.jpkmag.api.JPKMagDocumentValue;
import pl.puretech.jpkmag.api.JPKMagDocumentValuePosition;
import pl.puretech.jpkmag.internal.JPK;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Builder for simple adding PZ data to JPK_MAG
 * @author ewojewodka
 * @version 0.1.0
 */
public class JPKMagPZBuilder extends JPKMagDocumentBuilder<JPK.PZ> {

  JPKMagPZBuilder(JPKMagBuilder parent) {
    super(parent, JPKMagBuilder.FACTORY.createJPKPZ());
  }

  public JPKMagPZBuilder addWartosc(JPKMagDocumentValue value) {
    JPK.PZ.PZWartosc result = JPKMagBuilder.FACTORY.createJPKPZPZWartosc();

    result.setNumerPZ(value.getNumber());
    result.setDataPZ(value.getCreatedAt());
    result.setWartoscPZ(value.getValue());
    result.setDataOtrzymaniaPZ(value.getDocumentDate());
    result.setDostawca(value.getTargetFullName());
    result.setNumerFaPZ(value.getInvoiceCode());
    result.setDataFaPZ(value.getInvoiceDate());

    getDocument().getPZWartosc().add(result);
    return this;
  }

  public JPKMagPZBuilder addWiersz(JPKMagDocumentValuePosition value) {
    JPK.PZ.PZWiersz result = JPKMagBuilder.FACTORY.createJPKPZPZWiersz();

    result.setNumer2PZ(value.getDocumentNumber());
    result.setKodTowaruPZ(value.getPositionCode());
    result.setNazwaTowaruPZ(value.getName());
    result.setIloscPrzyjetaPZ(value.getAmount());
    result.setJednostkaMiaryPZ(value.getUnit());
    result.setCenaJednPZ(value.getPrice());
    result.setWartoscPozycjiPZ(value.getValue());
    getDocument().getPZWiersz().add(result);
    return this;
  }

  @Override
  public JPKMagDocumentBuilder<JPK.PZ> addSumaKontrolna(int amount, BigDecimal totalValue) {
    JPK.PZ.PZCtrl result = JPKMagBuilder.FACTORY.createJPKPZPZCtrl();

    result.setLiczbaPZ(new BigInteger(String.valueOf(amount)));
    result.setSumaPZ(totalValue);

    getDocument().setPZCtrl(result);
    return this;
  }

}
