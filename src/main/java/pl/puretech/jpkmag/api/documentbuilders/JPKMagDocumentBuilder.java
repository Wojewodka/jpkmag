package pl.puretech.jpkmag.api.documentbuilders;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.puretech.jpkmag.api.JPKMagDocumentValue;
import pl.puretech.jpkmag.api.JPKMagDocumentValuePosition;
import pl.puretech.jpkmag.internal.JPK;

import java.math.BigDecimal;

import static lombok.AccessLevel.PACKAGE;

/**
 * @param <T> JPK document type, i.e. {@link JPK.PZ}
 * @author ewojewodka
 * @version 0.1.0
 */
@RequiredArgsConstructor(access = PACKAGE)
public abstract class JPKMagDocumentBuilder<T> {

  /**
   * Parent builder which will be returned on {@link #end()}
   */
  private final JPKMagBuilder parent;

  /**
   * Instance of JPK_MAG document.
   * @see JPK.PZ
   * @see  JPK.WZ
   */
  @Getter
  private final T document;

  /**
   * End document and return main {@link JPKMagBuilder}
   */
  public JPKMagBuilder end() {
    return parent;
  }

  /**
   * Add data to document.
   * Please look at <a href="https://www.gov.pl/web/kas/struktury-jpk">JPK documentation</a>
   * for more details <br/>
   */
  public abstract JPKMagDocumentBuilder<T> addWartosc(@NonNull JPKMagDocumentValue value);

  /**
   * Add data to document.
   * Please look at <a href="https://www.gov.pl/web/kas/struktury-jpk">JPK documentation</a>
   * for more details <br/>
   */
  public abstract JPKMagDocumentBuilder<T> addWiersz(@NonNull JPKMagDocumentValuePosition position);

  /**
   * Add data to document.
   * Please look at <a href="https://www.gov.pl/web/kas/struktury-jpk">JPK documentation</a>
   * for more details <br/>
   * None of parameters can be null.
   */
  public abstract JPKMagDocumentBuilder<T> addSumaKontrolna(int amount, @NonNull BigDecimal totalValue);

}
