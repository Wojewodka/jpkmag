package pl.puretech.jpkmag.api.documentbuilders;


import pl.puretech.jpkmag.api.CompanyAddress;
import pl.puretech.jpkmag.api.CompanyData;
import pl.puretech.jpkmag.api.pojo.JPKMagTemplateData;
import pl.puretech.jpkmag.internal.*;

import static pl.puretech.jpkmag.api.JPKMagUtils.nullIfEmpty;

/**
 * Please look at <a href="https://www.gov.pl/web/kas/struktury-jpk">JPK documentation</a>
 * for more details <br/>
 * Main builder for JPK - it's some kind of overlay for JAXB generated classes.
 * Allow to customize {@link TNaglowek} and adding documents (PZ, WZ, RW). <br/>
 * @see #createPZ()
 * @see #createRW()
 * @see #createWZ()
 *
 * @author ewojewodka
 * @version 0.1.0
 *
 */
public class JPKMagBuilder {

  static final ObjectFactory FACTORY = new ObjectFactory();

  private final JPK jpk;

  /**
   * Create {@link JPKMagBuilder} with basic data.
   * @see JPKMagTemplateData
   * @param template data for {@link TNaglowek}
   */
  public JPKMagBuilder(JPKMagTemplateData template) {
    this.jpk = FACTORY.createJPK();
    jpk.setNaglowek(createNaglowek(template));
    jpk.setPodmiot1(createPodmiot(template.getPodmiot()));
    jpk.setMagazyn(template.getMagazyn());

  }

  private static TNaglowek createNaglowek(JPKMagTemplateData template) {
    TNaglowek result = FACTORY.createTNaglowek();
    TNaglowek.KodFormularza kodFormularza = FACTORY.createTNaglowekKodFormularza();
    kodFormularza.setValue(TKodFormularza.JPK_MAG);


    result.setKodFormularza(kodFormularza);
    result.setWariantFormularza(template.getWariantFormularza());
    result.setCelZlozenia(Byte.valueOf(template.getCelZlozenia().getXmlValue()));
    result.setDataWytworzeniaJPK(template.getDataWytworzenia());
    result.setDataOd(template.getDataOd());
    result.setDataDo(template.getDataDo());
    result.setDomyslnyKodWaluty(template.getDomyslnyKodWaluty());
    result.setKodUrzedu(template.getKodUrzedu().getCode());
    return result;
  }

  private static JPK.Podmiot1 createPodmiot(CompanyData companyData) {
    JPK.Podmiot1 result = FACTORY.createJPKPodmiot1();
    result.setAdresPodmiotu(createAdresPolski(companyData.getCompanyAddress()));
    result.setIdentyfikatorPodmiotu(createIdentyfikatorOsobyNiefizycznej(companyData));
    return result;
  }

  private static TIdentyfikatorOsobyNiefizycznej createIdentyfikatorOsobyNiefizycznej(CompanyData companyData) {
    TIdentyfikatorOsobyNiefizycznej result = FACTORY.createTIdentyfikatorOsobyNiefizycznej();
    result.setNIP(companyData.getVatId());
    result.setREGON(companyData.getRegon());
    result.setPelnaNazwa(companyData.getFullName());
    return result;
  }

  private static TAdresPolski createAdresPolski(CompanyAddress address) {
    TAdresPolski result = FACTORY.createTAdresPolski();
    result.setKodKraju(TKodKraju.fromValue(address.getCountryCode()));
    result.setWojewodztwo(nullIfEmpty(address.getProvince()));
    result.setUlica(nullIfEmpty(address.getStreet()));
    result.setMiejscowosc(nullIfEmpty(address.getPostCity()));
    result.setPowiat(nullIfEmpty(address.getCounty()));
    result.setGmina(nullIfEmpty(address.getMunicipality()));
    result.setNrDomu(nullIfEmpty(address.getHouse()));
    result.setNrLokalu(nullIfEmpty(address.getApartment()));
    result.setKodPocztowy(nullIfEmpty(address.getPostalCode()));
    result.setPoczta(nullIfEmpty(address.getPostOffice()));
    return result;
  }

  /**
   * Allow to add PZ to JPK_MAG. <br/>
   * Note that PZ is not required by XSD, but if exists then must has at least one PZWiersz na PZWartosc. <br/>
   * @see JPKMagPZBuilder
   */
  public JPKMagPZBuilder createPZ() {
    JPKMagPZBuilder jpkMagPZBuilder = new JPKMagPZBuilder(this);
    jpk.setPZ(jpkMagPZBuilder.getDocument());
    return jpkMagPZBuilder;
  }

  /**
   * Allow to add RW to JPK_MAG. <br/>
   * Note that RW is not required by XSD, but if exists then must has at least one RWWiersz na RWWartosc. <br/>
   * @see JPKMagRWBuilder
   */
  public JPKMagRWBuilder createRW() {
    JPKMagRWBuilder jpkMagRWBuilder = new JPKMagRWBuilder(this);
    jpk.setRW(jpkMagRWBuilder.getDocument());
    return jpkMagRWBuilder;
  }

  /**
   * Allow to add WZ to JPK_MAG. <br/>
   * Note that WZ is not required by XSD, but if exists then must has at least one WZWiersz na WZWartosc. <br/>
   * @see JPKMagWZBuilder
   */
  public JPKMagWZBuilder createWZ() {
    JPKMagWZBuilder builder = new JPKMagWZBuilder(this);
    jpk.setWZ(builder.getDocument());
    return builder;
  }


  /**
   * @return {@link JPK} instance.
   */
  public JPK build() {
    return jpk;
  }


}
