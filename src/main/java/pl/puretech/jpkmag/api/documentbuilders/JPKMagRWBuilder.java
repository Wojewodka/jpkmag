package pl.puretech.jpkmag.api.documentbuilders;

import pl.puretech.jpkmag.api.JPKMagDocumentValue;
import pl.puretech.jpkmag.api.JPKMagDocumentValuePosition;
import pl.puretech.jpkmag.internal.JPK;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Builder for simple adding RW data to JPK_MAG
 * @author ewojewodka
 * @version 0.1.0
 */
public class JPKMagRWBuilder extends JPKMagDocumentBuilder<JPK.RW> {

  JPKMagRWBuilder(JPKMagBuilder parent) {
    super(parent, JPKMagBuilder.FACTORY.createJPKRW());
  }

  public JPKMagRWBuilder addWartosc(JPKMagDocumentValue value) {
    JPK.RW.RWWartosc result = JPKMagBuilder.FACTORY.createJPKRWRWWartosc();

    result.setNumerRW(value.getNumber());
    result.setDataRW(value.getCreatedAt());
    result.setWartoscRW(value.getValue());
    result.setDataWydaniaRW(value.getDocumentDate());
    result.setSkadRW(value.getTargetFullName());

    getDocument().getRWWartosc().add(result);
    return this;
  }

  public JPKMagRWBuilder addWiersz(JPKMagDocumentValuePosition value) {
    JPK.RW.RWWiersz result = JPKMagBuilder.FACTORY.createJPKRWRWWiersz();

    result.setNumer2RW(value.getDocumentNumber());
    result.setKodTowaruRW(value.getPositionCode());
    result.setNazwaTowaruRW(value.getName());
    result.setIloscWydanaRW(value.getAmount());
    result.setJednostkaMiaryRW(value.getUnit());
    result.setCenaJednRW(value.getPrice());
    result.setWartoscPozycjiRW(value.getValue());

    getDocument().getRWWiersz().add(result);
    return this;
  }

  @Override
  public JPKMagDocumentBuilder<JPK.RW> addSumaKontrolna(int amount, BigDecimal totalValue) {
    JPK.RW.RWCtrl result = JPKMagBuilder.FACTORY.createJPKRWRWCtrl();

    result.setLiczbaRW(new BigInteger(String.valueOf(amount)));
    result.setSumaRW(totalValue);

    getDocument().setRWCtrl(result);
    return this;
  }
}
