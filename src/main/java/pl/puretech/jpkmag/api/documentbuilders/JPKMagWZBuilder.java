package pl.puretech.jpkmag.api.documentbuilders;

import pl.puretech.jpkmag.api.JPKMagDocumentValue;
import pl.puretech.jpkmag.api.JPKMagDocumentValuePosition;
import pl.puretech.jpkmag.internal.JPK;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Builder for simple adding WZ data to JPK_MAG
 * @author ewojewodka
 * @version 0.1.0
 */
public class JPKMagWZBuilder extends JPKMagDocumentBuilder<JPK.WZ> {

  JPKMagWZBuilder(JPKMagBuilder parent) {
    super(parent, JPKMagBuilder.FACTORY.createJPKWZ());
  }

  public JPKMagWZBuilder addWartosc(JPKMagDocumentValue value) {
    JPK.WZ.WZWartosc result = JPKMagBuilder.FACTORY.createJPKWZWZWartosc();

    result.setNumerWZ(value.getNumber());
    result.setDataWZ(value.getCreatedAt());
    result.setWartoscWZ(value.getValue());
    result.setDataWydaniaWZ(value.getDocumentDate());
    result.setOdbiorcaWZ(value.getTargetFullName());
    result.setNumerFaWZ(value.getInvoiceCode());
    result.setDataFaWZ(value.getInvoiceDate());

    getDocument().getWZWartosc().add(result);
    return this;
  }

  public JPKMagWZBuilder addWiersz(JPKMagDocumentValuePosition value) {
    JPK.WZ.WZWiersz result = JPKMagBuilder.FACTORY.createJPKWZWZWiersz();

    result.setNumer2WZ(value.getDocumentNumber());
    result.setKodTowaruWZ(value.getPositionCode());
    result.setNazwaTowaruWZ(value.getName());
    result.setIloscWydanaWZ(value.getAmount());
    result.setJednostkaMiaryWZ(value.getUnit());
    result.setCenaJednWZ(value.getPrice());
    result.setWartoscPozycjiWZ(value.getValue());

    getDocument().getWZWiersz().add(result);
    return this;
  }

  @Override
  public JPKMagDocumentBuilder<JPK.WZ> addSumaKontrolna(int amount, BigDecimal totalValue) {
    JPK.WZ.WZCtrl result = JPKMagBuilder.FACTORY.createJPKWZWZCtrl();

    result.setLiczbaWZ(new BigInteger(String.valueOf(amount)));
    result.setSumaWZ(totalValue);

    getDocument().setWZCtrl(result);
    return this;
  }
}
