package pl.puretech.jpkmag.api;

/**
 * <b>Main {@link Exception} for jpk-mag project.</b> <br/>
 * @author ewojewodka
 * @version 0.1.0
 */
public class JPKMagException extends Exception {

  private static final long serialVersionUID = -4215356998870073796L;

  public JPKMagException(Throwable cause) {
    super(cause);
  }

  public JPKMagException(String msg, Throwable cause) {
    super(msg, cause);
  }

  public JPKMagException(String msg) {
    super(msg);
  }
}
