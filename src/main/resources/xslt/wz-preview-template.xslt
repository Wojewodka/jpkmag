<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:jpk="http://jpk.mf.gov.pl/wzor/2016/03/09/03093/"
>

  <xsl:template name="wz-preview-tab">
    <div class="tab">
      <input type="radio" id="tab-wz" name="jpk" />
      <label for="tab-wz">WZ</label>
      <div class="content">
        <table class="doc-table">
          <thead>
            <tr>
              <th>Numer</th>
              <th>Kod Towaru</th>
              <th>Nazwa Towaru</th>
              <th>Ilość Wydana</th>
              <th>Jednostka Miary</th>
              <th>Cena Jednostkowa</th>
              <th>Wartość Pozycji</th>
            </tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="//jpk:WZWiersz"/>
          </tbody>
        </table>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="jpk:WZWiersz">
    <xsl:variable name="NumerWZ" select="jpk:Numer2WZ" />
    <tr class="document">
      <td><xsl:value-of select="jpk:Numer2WZ" /></td>
      <td><xsl:value-of select="jpk:KodTowaruWZ" /></td>
      <td><xsl:value-of select="jpk:NazwaTowaruWZ" /></td>
      <td><xsl:value-of select="jpk:IloscWydanaWZ" /></td>
      <td><xsl:value-of select="jpk:JednostkaMiaryWZ" /></td>
      <td><xsl:value-of select="jpk:CenaJednWZ" /></td>
      <td><xsl:value-of select="jpk:WartoscPozycjiWZ" /></td>
    </tr>
    <tr class="document-items">
      <td colspan="7">
        <table class="item-table">
          <thead>
            <tr>
              <th>Data</th>
              <th>Wartość</th>
              <th>Data Otrzymania</th>
              <th>Dostawca</th>
            </tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="//jpk:WZWartosc[./jpk:NumerWZ[text()=$NumerWZ]]"/>
          </tbody>
        </table>
      </td>
    </tr>
  </xsl:template>
  <xsl:template match="jpk:WZWartosc">
    <tr>
      <td><xsl:value-of select="jpk:DataWZ" /></td>
      <td><xsl:value-of select="jpk:WartoscWZ" /></td>
      <td><xsl:value-of select="jpk:DataWydaniaWZ" /></td>
      <td><xsl:value-of select="jpk:OdbiorcaWZ" /></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
