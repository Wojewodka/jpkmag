<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:jpk="http://jpk.mf.gov.pl/wzor/2016/03/09/03093/"
>

<xsl:template name="rw-preview-tab">
    <div class="tab">
      <input type="radio" id="tab-rw" name="jpk" />
      <label for="tab-rw">RW</label>
      <div class="content">
        <table class="doc-table">
          <thead>
            <tr>
              <th>Numer</th>
              <th>Kod Towaru</th>
              <th>Nazwa Towaru</th>
              <th>Ilość Przyjęta</th>
              <th>Jednostka Miary</th>
              <th>Cena Jednostkowa`</th>
              <th>Wartość Pozycji</th>
            </tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="//jpk:RWWiersz"/>
          </tbody>
        </table>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="jpk:RWWiersz">
    <xsl:variable name="NumerRW" select="jpk:Numer2RW" />
    <tr class="document">
      <td><xsl:value-of select="jpk:Numer2RW" /></td>
      <td><xsl:value-of select="jpk:KodTowaruRW" /></td>
      <td><xsl:value-of select="jpk:NazwaTowaruRW" /></td>
      <td><xsl:value-of select="jpk:IloscWydanaRW" /></td>
      <td><xsl:value-of select="jpk:JednostkaMiaryRW" /></td>
      <td><xsl:value-of select="jpk:CenaJednRW" /></td>
      <td><xsl:value-of select="jpk:WartoscPozycjiRW" /></td>
    </tr>
    <tr class="document-items">
      <td colspan="7">
        <table class="item-table">
          <thead>
            <tr>
              <th>Data</th>
              <th>Wartość</th>
              <th>Data Otrzymania</th>
              <th>Dostawca</th>
            </tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="//jpk:RWWartosc[./jpk:NumerRW[text()=$NumerRW]]"/>
          </tbody>
        </table>
      </td>
    </tr>
  </xsl:template>
  <xsl:template match="jpk:RWWartosc">
    <tr>
      <td><xsl:value-of select="jpk:DataRW" /></td>
      <td><xsl:value-of select="jpk:WartoscRW" /></td>
      <td><xsl:value-of select="jpk:DataWydaniaRW" /></td>
      <td><xsl:value-of select="jpk:SkadRW" /></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
