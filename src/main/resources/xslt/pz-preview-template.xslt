<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:jpk="http://jpk.mf.gov.pl/wzor/2016/03/09/03093/"
>

  <xsl:template name="pz-preview-tab">
    <div class="tab">
      <input type="radio" id="tab-pz" name="jpk" checked="true" />
      <label for="tab-pz">PZ</label>
      <div class="content">
        <table class="doc-table">
          <thead>
            <tr>
              <th>Numer</th>
              <th>Kod Towaru</th>
              <th>Nazwa Towaru</th>
              <th>Ilość Przyjeta</th>
              <th>Jednostka Miary</th>
              <th>Cena Jednostkowa</th>
              <th>Wartość Pozycji</th>
            </tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="//jpk:PZWiersz"/>
          </tbody>
        </table>
      </div>
    </div>
  </xsl:template>
  
  <xsl:template match="jpk:PZWiersz">
    <xsl:variable name="NumerPZ" select="jpk:Numer2PZ" />
    <tr class="document">
      <td><xsl:value-of select="jpk:Numer2PZ" /></td>
      <td><xsl:value-of select="jpk:KodTowaruPZ" /></td>
      <td><xsl:value-of select="jpk:NazwaTowaruPZ" /></td>
      <td><xsl:value-of select="jpk:IloscPrzyjetaPZ" /></td>
      <td><xsl:value-of select="jpk:JednostkaMiaryPZ" /></td>
      <td><xsl:value-of select="jpk:CenaJednPZ" /></td>
      <td><xsl:value-of select="jpk:WartoscPozycjiPZ" /></td>
    </tr>
    <tr class="document-items">
      <td colspan="7">
        <table class="item-table">
          <thead>
            <tr>
              <th>Data</th>
              <th>Wartość</th>
              <th>Data Otrzymania</th>
              <th>Dostawca</th>
              <th>Numer FV</th>
              <th>Data FV</th>
            </tr>
          </thead>
          <tbody>
            <xsl:apply-templates select="//jpk:PZWartosc[./jpk:NumerPZ[text()=$NumerPZ]]"/>
          </tbody>
        </table>
      </td>
    </tr>
  </xsl:template>
  <xsl:template match="jpk:PZWartosc">
    <tr>
      <td><xsl:value-of select="jpk:DataPZ" /></td>
      <td><xsl:value-of select="jpk:WartoscPZ" /></td>
      <td><xsl:value-of select="jpk:DataOtrzymaniaPZ" /></td>
      <td><xsl:value-of select="jpk:Dostawca" /></td>
      <td><xsl:value-of select="jpk:NumerFaPZ" /></td>
      <td><xsl:value-of select="jpk:DataFaPZ" /></td>
    </tr>
  </xsl:template>
</xsl:stylesheet>
