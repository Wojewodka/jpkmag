<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  >
  <xsl:import href="pz-preview-template.xslt"/>
  <xsl:import href="wz-preview-template.xslt"/>
  <xsl:import href="rw-preview-template.xslt"/>

  <xsl:output method="html" indent="yes" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
    <html>
      <head>
        <style>
            <![CDATA[
            /*
              Styles are included inline because there're problems
              with using css from external file if templates are used by external projects included to fat jar.
            */
            @import url('https://fonts.googleapis.com/css?family=Lato&display=swap');


            ::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 0.45rem !important;
            height: 0.45rem !important;
            background-color: #F5F5F5 !important;
            }

            ::-webkit-scrollbar-thumb {
            -webkit-appearance: none;
            background: rgba(0, 0, 0, 0.5) !important;
            transition: color .2s ease !important;
            }

            ::-webkit-scrollbar-track {
            -webkit-appearance: none;
            background: rgba(0, 0, 0, 0.1) !important;
            border-radius: 0 !important;
            }


            body {
            width: 100%;
            height: 100%;
            font-family: Lato;
            font-weight: 400;
            }

            table {
            border-collapse: collapse;
            font-size: 1rem;
            }

            .tabs {
            display: grid;
            grid-template-columns: 33% 33% 33%;
            }

            .tab {
            display: flex;
            }

            thead {
            background: #f9fafb;
            }

            .content {
            display: none;
            width: 100vw;
            position: absolute;
            left: 0;
            top: 35px;
            }

            .doc-table {
            border-top: .2em solid #2185d0;
            border-radius: .28571429rem;
            }

            td, th, tr {
            padding: .92857143em .78571429em;
            border-left: 1px solid rgba(34,36,38,.1);
            border-bottom: 1px solid rgba(34,36,38,.1)
            }


            input[type="radio"] {
            opacity: 0;
            width: 0;
            }

            input[type="radio"]:checked + label + .content {
            display: block;
            }

            label {
            cursor: pointer !important;
            width: 100%;
            text-align: center;
            height: 30px;
            line-height: 30px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            transition-duration: .2s;
            }

            input[type="radio"]:checked ~ label,
            label:hover {
            background-color: #2185d0;
            color: white;
            }

            input[type="radio"] + label {
            width: 100%;
            }

            .doc-table, .item-table {
            width: 100%;
            }

            .item-table td {
            background-color: #f9fafb;
            }

            .document-items td:first-of-type {
            padding: 0;
            }
             ]]>
        </style>
      </head>
    <body>
      <div class="tabs">

        <xsl:call-template name="pz-preview-tab" />
        <xsl:call-template name="wz-preview-tab" />
        <xsl:call-template name="rw-preview-tab" />

      </div>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
