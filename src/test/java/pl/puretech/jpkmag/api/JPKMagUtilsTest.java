package pl.puretech.jpkmag.api;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static pl.puretech.jpkmag.api.JPKMagUtils.isEmpty;
import static pl.puretech.jpkmag.api.JPKMagUtils.nullIfEmpty;

public class JPKMagUtilsTest {

  @Test
  public void toYYYYMMDDTest() {
    LocalDate now = JPKMagUtils.parseToLocalDate("2019-06-12");
    assertEquals("2019-06-12", JPKMagUtils.toYYYYMMDD(now));
  }

  @Test
  public void parseToLocalDateTest() {
    LocalDate ld = JPKMagUtils.parseToLocalDate("2019-06-12");
    assertEquals(2019, ld.getYear());
    assertEquals(06, ld.getMonthValue());
    assertEquals(12, ld.getDayOfMonth());
  }

  @Test
  public void parseToLocalDateTimeTest() {
    LocalDateTime ldt = JPKMagUtils.parseToLocalDateTime("2019-06-12T01:02:03.123");

    assertEquals(2019, ldt.getYear());
    assertEquals(06, ldt.getMonthValue());
    assertEquals(12, ldt.getDayOfMonth());
    assertEquals(01, ldt.getHour());
    assertEquals(02, ldt.getMinute());
    assertEquals(03, ldt.getSecond());
    assertEquals(123000000, ldt.getNano()); // look at #getNano java doc
  }

  @Test
  public void isEmptyTest() {
    assertTrue(isEmpty(null));
    assertTrue(isEmpty(""));
    assertTrue(isEmpty(" "));
    assertFalse(isEmpty("lol"));
  }

  @Test
  public void nullIfEmptyTest() {
    assertNull(nullIfEmpty(""));
    assertNotNull(nullIfEmpty("lol"));
  }

}
