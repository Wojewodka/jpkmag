package pl.puretech.jpkmag.api;

import org.junit.Test;
import pl.puretech.jpkmag.api.consts.CelZlozenia;
import pl.puretech.jpkmag.api.consts.TKodUS;
import pl.puretech.jpkmag.api.documentbuilders.JPKMagBuilder;
import pl.puretech.jpkmag.api.pojo.JPKMagCompanyAddress;
import pl.puretech.jpkmag.api.pojo.JPKMagCompanyData;
import pl.puretech.jpkmag.api.pojo.JPKMagTemplateData;
import pl.puretech.jpkmag.api.service.JPKMagGeneratorType;
import pl.puretech.jpkmag.internal.TKodKraju;
import pl.puretech.jpkmag.objects.TestDocumentValue;
import pl.puretech.jpkmag.objects.TestDocumentValuePosition;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class JPKMagBuilderTest {

  private static final JPKMagBuilder data;

  static {
    JPKMagTemplateData templateData = JPKMagTemplateData.builder()
        .celZlozenia(CelZlozenia.PIERWSZA_DEKLARACJA)
        .dataOd(LocalDate.now().minusDays(30))
        .dataDo(LocalDate.now())
        .kodUrzedu(TKodUS.getByCode("2203"))
        .magazyn("test")
        .podmiot(buildExampleCompany())
        .build();

    data = new JPKMagBuilder(templateData)
        .createPZ()
        .addWartosc(new TestDocumentValue())
        .addWartosc(new TestDocumentValue())
        .addWiersz(new TestDocumentValuePosition())
        .addWiersz(new TestDocumentValuePosition())
        .addSumaKontrolna(2, new BigDecimal(LocalDateTime.now().getNano()))
        .end()
        .createRW()
        .addWartosc(new TestDocumentValue())
        .addWartosc(new TestDocumentValue())
        .addWiersz(new TestDocumentValuePosition())
        .addWiersz(new TestDocumentValuePosition())
        .addSumaKontrolna(2, new BigDecimal(LocalDateTime.now().getNano()))
        .end()
        .createWZ()
        .addWartosc(new TestDocumentValue())
        .addWartosc(new TestDocumentValue())
        .addWiersz(new TestDocumentValuePosition())
        .addWiersz(new TestDocumentValuePosition())
        .addSumaKontrolna(2, new BigDecimal(LocalDateTime.now().getNano()))
        .end();
  }

  private static CompanyData buildExampleCompany() {
    return JPKMagCompanyData.builder()
        .nip("1234567980")
        .pelnaNazwa("Testowa pełna nazwa")
        .regon("123456789")
        .adresPodmiotu(buildExampleCompanyAddress())
        .build();
  }

  private static CompanyAddress buildExampleCompanyAddress() {
    return JPKMagCompanyAddress.builder()
        .kodKraju(TKodKraju.PL)
        .wojewodztwo("Dolnośląskie")
        .powiat("Wrocławski")
        .gmina("Wrocław")
        .ulica("Biskupia")
        .nrDomu("2")
        .nrLokalu("4-5")
        .miejscowosc("Wrocław")
        .kodPocztowy("00-001")
        .poczta("Wrocław")
        .build();
  }

  @Test
  public void JPKMagXMLSuccessTest() throws JPKMagException {
    JPKMagGeneratorType.XML.getGenerator().generate(data, System.out);

  }

  @Test
  public void JPKMagHTMLBuilderSuccessTest() throws JPKMagException {
    JPKMagGeneratorType.HTML.getGenerator().generate(data, System.out);
  }

}
