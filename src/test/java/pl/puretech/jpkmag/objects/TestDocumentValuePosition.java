package pl.puretech.jpkmag.objects;

import pl.puretech.jpkmag.api.JPKMagDocumentValuePosition;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class TestDocumentValuePosition implements JPKMagDocumentValuePosition {
  @Override
  public String getDocumentNumber() {
    return "example/number/" + LocalDateTime.now().getSecond();
  }

  @Override
  public String getPositionCode() {
    return UUID.randomUUID().toString();
  }

  @Override
  public String getName() {
    return "Testowa nazwa nr " + LocalDateTime.now().getSecond();
  }

  @Override
  public BigDecimal getAmount() {
    return BigDecimal.valueOf(LocalDateTime.now().getNano());
  }

  @Override
  public String getUnit() {
    return "Metry";
  }

  @Override
  public BigDecimal getPrice() {
    return BigDecimal.valueOf(LocalDateTime.now().getNano());
  }

  @Override
  public BigDecimal getValue() {
    return BigDecimal.valueOf(LocalDateTime.now().getNano());
  }
}
