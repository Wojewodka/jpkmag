package pl.puretech.jpkmag.objects;

import pl.puretech.jpkmag.api.JPKMagDocumentValue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class TestDocumentValue implements JPKMagDocumentValue {

  @Override
  public String getNumber() {
    return "example/number/" + LocalDateTime.now().getSecond();
  }

  @Override
  public LocalDate getCreatedAt() {
    return LocalDate.now();
  }

  @Override
  public BigDecimal getValue() {
    return BigDecimal.valueOf(LocalDateTime.now().getNano());
  }

  @Override
  public LocalDate getDocumentDate() {
    return LocalDate.now();
  }

  @Override
  public String getTargetFullName() {
    return "Przykładowa spółka z.o.o";
  }

  @Override
  public String getInvoiceCode() {
    return "FV001/09/2019";
  }

  @Override
  public LocalDate getInvoiceDate() {
    return LocalDate.now();
  }
}
